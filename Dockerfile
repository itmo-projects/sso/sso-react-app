FROM node:18.12.1-alpine3.16 as BUILD

ARG KEYCLOAK_URL
ARG REALM
ARG CLIENT_ID
ARG BASE_URL

ENV REACT_APP_KEYCLOAK_URL $KEYCLOAK_URL
ENV REACT_APP_REALM $REALM
ENV REACT_APP_CLIENT_ID $CLIENT_ID
ENV REACT_APP_BASE_URL $BASE_URL

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY ./ .
RUN npm run build

FROM nginx:1.21.0-alpine as PRODUCTION

COPY --from=BUILD /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8081
CMD ["nginx", "-g", "daemon off;"]