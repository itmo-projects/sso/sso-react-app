# React App Example for SSO Project
### Университет ИТМО | ITMO University

#### Настройка среды | Setting Up the Environment
Выставляем IP для Resource Server (BASE_URL) и Auth Server (KEYCLOAK_URL) </br>
We will refence it after
```shell
export KEYCLOAK_URL=<host:port keycloak> &&
export REALM=<realm name> &&
export CLIENT_ID=<client id> &&
export BASE_URL=<backend url>
```

## Запуск локально через `npm run start`
```shell
REACT_APP_KEYCLOAK_URL="$KEYCLOAK_URL" &&
REACT_APP_REALM="$REALM" &&
REACT_APP_CLIENT_ID="$CLIENT_ID" &&
REACT_APP_BASE_URL="$BASE_URL"
```

### Запуск в Docker Container

```shell
docker build \
 --build-arg KEYCLOAK_URL \
 --build-arg REALM \
 --build-arg CLIENT_ID \
 --build-arg BASE_URL \
 --tag sso-react-app:latest .
```

Запуск контейнера на 8081 порту
```shell
docker run -d \
        --name sso-react-app \
        -d \
        -p 8081:8081 sso-react-app:latest
```

#### Пример запуска
```shell
export KEYCLOAK_URL=http://172.17.0.2:8180 &&
export REALM=sso-itmo &&
export CLIENT_ID=sso-itmo-client &&
export BASE_URL=http://172.17.0.3:9000
```

### Получить IP-адрес контейнера
`docker inspect sso-react-app --format '{{.NetworkSettings.IPAddress}}'`