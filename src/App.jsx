import logo from './logo.png';
import './App.css';
import NavBar from './components/NavBar';
import RenderOnAnonymous from "./components/RenderOnAnonymous";
import RenderOnAuthenticated from "./components/RenderOnAuthenticated";
import Welcome from "./components/Welcome";

function App() {
  return (
    <div className="app">
      <RenderOnAnonymous>
        <Welcome />
      </RenderOnAnonymous>
      <RenderOnAuthenticated>
        <NavBar />
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
      </RenderOnAuthenticated>
    </div>
  );
}

export default App;
