import UserService from "../services/UserService";

export default function NavBar() {
  return (
    <nav>
      <div className='flex justify-around items-center py-5 bg-[#234] text-white'>
        <h1 className='font-semibold font-2xl'>KeyCloak App</h1>
        <button className="btn btn-lg btn-warning" onClick={() => UserService.doLogout()}>Logout</button>
      </div>
    </nav>
  )
}