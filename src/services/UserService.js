import Keycloak from "keycloak-js";

const kcConfig = {
  url: process.env.REACT_APP_KEYCLOAK_URL, // change to local 
  realm: process.env.REACT_APP_REALM,
  clientId: process.env.REACT_APP_CLIENT_ID,
}

const _kc = new Keycloak(kcConfig);

_kc.onTokenExpired = function () {

  //Для Василия, чтобы он понимал, когда происходит рефреш
  debugger;

  _kc.updateToken(180)
    .then(function (refreshed) {
      if (refreshed) {
        alert('Token refreshed');
        // write any code you required here
      } else {
        alert('Token is still valid now');
      }
    }).catch(function () {
    alert('Failed to refresh the token, or the session has expired');
  });
}

/**
 * Initializes Keycloak instance and calls the provided callback function if successfully authenticated.
 *
 * @param onAuthenticatedCallback
 */
const initKeycloak = (onAuthenticatedCallback) => {
  _kc.init({
    onLoad: 'login-required',
  })
    .then((authenticated) => {
      if (!authenticated) {
        console.log("user is not authenticated..!");
      }
      onAuthenticatedCallback();
    })
    .catch(console.error);
};

const doLogin = _kc.login;

const doLogout = _kc.logout;

const getToken = () => _kc.token;

const isLoggedIn = () => !!_kc.token;

const updateToken = (successCallback) =>
  _kc.updateToken(5)
    .then(successCallback)
    .catch(doLogin);

const getUsername = () => _kc.tokenParsed?.preferred_username;

const hasRole = (roles) => roles.some((role) => _kc.hasRealmRole(role));

const UserService = {
  initKeycloak,
  doLogin,
  doLogout,
  isLoggedIn,
  getToken,
  updateToken,
  getUsername,
  hasRole,
};

export default UserService;
